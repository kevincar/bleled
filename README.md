# BLE LED

Control an LED over BLE using Python and a BLE-enabled Adafruit Feather32u4

# Setup

## Hardware

Connect an LED to pin 9 of the adafruit board (see image).

<img src="img/hw.png" width="300px" />

## Software

First clone the repository to get the code

```bash
# Clone the repository
git clone https://gitlab.com/kevincar/bleled
cd bleled
```

### Adafruit

Upload `ino/blelight.ino` onto the Adafruit board.

Keep the board running the software and complete the computer steps below.

### Computer

This has been tested on both macOS and Windows 11.

```bash
# Create the environment to install python dependencies
conda env create
conda activate feather

# run the python script
python -m feather
```

If the script runs, you the LED will alternate between on and off every second
