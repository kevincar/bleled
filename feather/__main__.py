import logging
import asyncio
from bleak import BleakScanner, BleakClient

logger = logging.getLogger(name=__name__)
logging.basicConfig(level=logging.DEBUG)

device_name = "Adafruit Bluefruit LE"
device_address = ""
char_uuid = "2345"

async def main(device_address):
    if device_address == "":
        logger.debug("No device address provided. Searching...")
        # scan for devices
        devices = await BleakScanner.discover()
    
        # find our device
        potential_device = [
            device
            for device in devices
            if device.name == device_name
        ]

        if len(potential_device) < 1:
            raise Exception(f"Failed to find device: {device_name}")

        device = potential_device[0]
        device_address = device.address
        logger.info(f"Found device address: {device_address}")

    # connect
    logger.debug("Connecting...")
    async with BleakClient(device_address) as client:
        logger.debug("Connected!")
        # Read Value
        full_uuid = f"0000{char_uuid}-0000-1000-8000-00805F9B34FB"
        value = await client.read_gatt_char(full_uuid)
        logger.debug(f"value: {value}")

        light_on = False
        # Start alternating
        while True:
            light_on = not light_on
            int_val = int(light_on)
            byte_val = int_val.to_bytes(length=1, byteorder="little")
            await client.write_gatt_char(full_uuid, byte_val, True)
            await asyncio.sleep(1)

if __name__ == "__main__":
    asyncio.run(main(device_address))
