#include "Adafruit_BluefruitLE_SPI.h"
#include "BluefruitConfig.h"

int32_t charid;

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

void error(const __FlashStringHelper* err) {
  Serial.println(err);
  while (1)
    ;
}

void callback(int32_t id, uint8_t data[], uint16_t len) {
  if (id == charid) {
    if (data[0] == 1) {
      Serial.println("ON");
      digitalWrite(9, HIGH);
    } else {
      Serial.println("OFF");
      digitalWrite(9, LOW);
    }
  }
}

void setup() {
  delay(500);

  // Setup Serial communication
  Serial.begin(115200);
  Serial.println("Adafruit Bluefruit Command Mode Example");
  Serial.println("---------------------------------------");

  // Initialize Pins
  pinMode(9, OUTPUT);

  // Initialize Bluetooth Module
  Serial.print("Initialising the Bluefruit LE module");

  if (!ble.begin(VERBOSE_MODE)) {
    error(F("Couldn't find Bluefruit! Make sure it's in Command mode & check wiring"));
  }
  Serial.println("OK!");

  ble.echo(false);

  Serial.println("Adding service");
  ble.sendCommandCheckOK("AT+GATTCLEAR");
  ble.sendCommandCheckOK("AT+GATTADDSERVICE=uuid=0x1234");
  ble.sendCommandWithIntReply(F("AT+GATTADDCHAR=UUID=0x2345,PROPERTIES=0x1A,MIN_LEN=1,MAX_LEN=1,DATATYPE=STRING,DESCRIPTION=CTRL,VALUE=0"), &charid);

  Serial.print("Obtain characteristic handle ID: ");
  Serial.println(charid);

  ble.reset();

  ble.echo(false);

  Serial.println("Setting callbacks...");

  ble.setBleGattRxCallback(charid, callback);
}

void loop() {
  // put your main code here, to run repeatedly:
  ble.update(200);
}
